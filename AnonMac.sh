#!/bin/bash
## AnonMac
## dipendence: tor
## sudo port install tor
## ctrl-c for stop 

function ctrl_c() {
sudo networksetup -setsocksfirewallproxy "Ethernet" "" ""
sudo networksetup -setsocksfirewallproxy "Wi-Fi" "" ""
sudo networksetup -setsocksfirewallproxystate "Wi-Fi" off
sudo networksetup -setsocksfirewallproxystate "Ethernet"  off
sudo networksetup -setdnsservers Wi-Fi 8.8.8.8 8.8.4.4
sudo networksetup -setdnsservers Ethernet 8.8.8.8 8.8.4.4
}

trap ctrl_c INT
ctrl_c
sudo networksetup -setsocksfirewallproxy "Ethernet" 127.0.0.1 9050 
sudo networksetup -setsocksfirewallproxystate "Ethernet" on
sudo networksetup -setsocksfirewallproxy "Wi-Fi" 127.0.0.1 9050 
sudo networksetup -setsocksfirewallproxystate "Wi-Fi" on
sudo networksetup -setdnsservers Wi-Fi 8.8.8.8 8.8.4.4
sudo networksetup -setdnsservers Ethernet 8.8.8.8 8.8.4.4
tor





